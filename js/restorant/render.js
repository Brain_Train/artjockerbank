class Render {
    constructor(data) {
        this.modalSwitch = null;
        this.restorauntData = data;
        this.buttonOpenModal = document.querySelector('.apperButton');
        this.allCard = document.querySelector('.blockCard');
        this.modal = document.querySelector('.modal');
        this.modalCard = document.querySelector('.modal__modalCard');
        this.information = document.querySelector('.allInfo');
        this.renderCard();
        this.informationRestoraunt();
    }

    informationRestoraunt() {
        let salaryesData = restorauntResult.countingSalariesEachDepartment();
        let avarageSalaryesData = restorauntResult.countingAverageSalaryDepartment();
        let postFired = restorauntResult.menedgerFired(('Meneger'));
        let isFired = restorauntResult.firedAmount(true);

        const allSalaryInformation = document.createElement('div');
        allSalaryInformation.className = 'allSalary';

        const avarageInformation = document.createElement('div');
        avarageInformation.className = 'avarageInformation';

        const firedMenegerAmount = document.createElement('div');
        firedMenegerAmount.className = 'firedMeneger';

        const firedEmployyer = document.createElement('div');
        firedEmployyer.className = 'firedEmployyer';

        for ( let i in salaryesData ) {
            allSalaryInformation.innerHTML += `
            <p class="information"> ${i}: ${salaryesData[i]} </p>
            `;
        }

        for ( let i in avarageSalaryesData ) {
            avarageInformation.innerHTML += `
            <p class="information"> ${i}: ${avarageSalaryesData[i]} </p>
            `;
        }

        for ( let i in postFired ) {
            firedMenegerAmount.innerHTML += `
            <p class="information"> ${i}: ${postFired[i]} </p>
            `;
        }

        for ( let i in isFired ) {
            firedEmployyer.innerHTML += `
            <p class="information"> ${i}: ${isFired[i]} </p>
            `;
        }
        
        this.information.append(avarageInformation);
        this.information.append(allSalaryInformation);
        this.information.append(firedMenegerAmount);
        this.information.append(firedEmployyer);
    }

    renderCards(data, index) {
        const card = document.createElement('div');
        card.className = 'card';
        card.innerHTML = `
        <h3 class="allLable">${data.name} ${data.surname}</h3>
        <p class="infText"> ${data.department}</p>
        <p class="infText">salary: ${data.salary}$</p>
        <p class="infText">post: ${data.post}</p>
        <p class="infText">status: ${data.fired ? 'fired' : 'working'}</p>
        `;
  
        const buttonChange = document.createElement('button');
        buttonChange.className = 'cardButton';
        buttonChange.innerHTML = 'Change';
        card.appendChild(buttonChange);
        buttonChange.addEventListener('click', this.openChangingModal.bind(this, data, index));

        this.buttonOpenModal.addEventListener('click', this.openCreatingModal.bind(this, data));

        const buttonDelete = document.createElement('button');
        buttonDelete.className = 'cardButtonDelete';
        buttonDelete.innerHTML = 'Delete';
        card.appendChild(buttonDelete);
        buttonDelete.addEventListener('click', this.deleteUsers.bind(this, index));

        return card;
    }

    openModal(data, index) {
        this.modal.classList.add('active');
        this.modalCard.innerHTML = `
        <div class="changes">
            <p> Name </p>
            <input class="inputChanges" type="text" placeholder="Enter name..." name="name"> 
        </div>
        <div class="changes">
            <p> Surname </p>
            <input class="inputChanges" type="text" placeholder="Enter surname..." name="surname"> 
        </div>
        <div class="changes">
            <p> Department </p>
            <input class="inputChanges" type="text" placeholder="Enter department..." name="department"> 
        </div>
        <div class="changes">
            <p> Salary </p>
            <input class="inputChanges" type="text" placeholder="Enter salary..." name="salary"> 
        </div>
        <div class="changes">
            <p> Post </p>
            <input class="inputChanges" type="text" placeholder="Enter post..." name="post"> 
        </div>
        <div class="changes">
            <p> Fired </p>
            <input class="check" type="checkbox" placeholder="Enter fired..." name="fired"> 
        </div>
        `;

        const divForApplyButton = document.createElement('div');
        divForApplyButton.className = 'applyButton';
        this.modalCard.append(divForApplyButton);

        const button = document.createElement('button');
        button.className = 'editeEmploeerButton';
        button.innerText = 'Apply changes';
        divForApplyButton.appendChild(button);

        divForApplyButton.addEventListener('click', this.editeEmployeer.bind(this, index));

        this.modalCard.addEventListener('click', this.stopPropagation.bind(this));
        this.modal.addEventListener('click', this.closeModal.bind(this));
    }

    createUser(event) {
        event.preventDefault();

        let newEmpoloyeer = new FormData(event.target.closest('form'));  

        this.restorauntData.unshift({
            department: newEmpoloyeer.get('department') || 'Please enter department',
            name: newEmpoloyeer.get('name') || 'Name',
            surname: newEmpoloyeer.get('surname') || 'Surname',
            salary: Number(newEmpoloyeer.get('salary')) || 'Change salary',
            post: newEmpoloyeer.get('post') || 'Change post',
            fired: newEmpoloyeer.get('fired') || null,
        });

        this.renderCard();
    
    }

    renderCard() {
        this.allCard.innerHTML = ' ';
        const cards = this.restorauntData.map((data, index) => this.renderCards(data, index));
        this.allCard.append(...cards); 
    }

    changeUsers(event, i) {
        event.preventDefault();

        let changeEmployeer = new FormData(event.target.closest('form'));
        
        this.restorauntData[i].name =  changeEmployeer.get('name') || this.restorauntData[i].name;
        this.restorauntData[i].surname =  changeEmployeer.get('surname') || this.restorauntData[i].surname;
        this.restorauntData[i].department =  changeEmployeer.get('department') || this.restorauntData[i].department;
        this.restorauntData[i].salary =  changeEmployeer.get('salary') || this.restorauntData[i].salary;
        this.restorauntData[i].post =  changeEmployeer.get('post') || this.restorauntData[i].post;
        this.restorauntData[i].fired =  changeEmployeer.get('fired') || this.restorauntData[i].fired;

        this.renderCard();
        
    }

    deleteUsers(index) {
        this.restorauntData.splice(index, 1);
        this.renderCard();
    }

    closeModal() {
        this.modal.classList.remove('active');
    }

    openChangingModal(data, index) {
        this.modalSwitch = true;
        this.openModal(data, index);
    }

    editeEmployeer(index, event) {
        this.information.innerHTML = ' ';
        if(this.modalSwitch === true){
            this.changeUsers(event, index);
        } else if(this.modalSwitch === false) {
            this.createUser(event);
        }
        this.closeModal();
        this.informationRestoraunt();
    }

    openCreatingModal(data) {
        this.modalSwitch = false;
        this.openModal(data);
    }

    stopPropagation(element) {
        element.stopPropagation();
    }

}

const render = new Render(employerArr);
