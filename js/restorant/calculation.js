class RestorauntCalculation {
    constructor(data) {
        this.restorauntData = data;
    }

    countingSalariesEachDepartment() {
        let allSalary = {};

        this.restorauntData.forEach((value) => {
            allSalary[value.department] = allSalary[value.department] || 0;
            allSalary[value.department] += value.salary;
        });

        return allSalary;
    }

    countingAverageSalaryDepartment() {
        let avarageSalary = [];

        this.restorauntData.forEach((item) => {
            avarageSalary[item.department] = avarageSalary[item.department] || [];

            for (let i in avarageSalary) {
                if (i === item.department) {
                    avarageSalary[i].push(item.salary);
                }
            }
        });

        function average(name, items) {
            let sum = 0;

            for (let i = 0; i < items.length; i++) {
                sum += items[i];
            }

            avarageSalary[name] = Math.round(sum / items.length);
        }

        for (let index in avarageSalary) {
            average(index, avarageSalary[index]);
        }

        return avarageSalary;
    }

    getSalary(colback) {
        let departmentSalary = [];
        let postSalary = [];
        let salaryArray = [];

        this.restorauntData.forEach((value) => {
            departmentSalary[value.department] = departmentSalary[value.department] || [];
            for (let i in departmentSalary) {
                if (i === value.department) {
                    departmentSalary[i].push(value.salary);
                }
            }
        });

        for (let i in departmentSalary) {
            departmentSalary[i].sort(colback);
            departmentSalary[i] = departmentSalary[i].shift();
        }

        this.restorauntData.forEach((value) => {
            postSalary[value.post] = postSalary[value.post] || [];
            for (let i in postSalary) {
                if (i === value.post) {
                    postSalary[i].push(value.salary);
                }
            }
        });

        for (let i in postSalary) {
            postSalary[i].sort(colback);
            postSalary[i] = postSalary[i].shift();
        }
        salaryArray.push({
            departmentSalary: departmentSalary
        }, {
            postSalary: postSalary
        });
        
        return salaryArray;
    }

    menedgerFired(post) {
        let result = [];

        this.restorauntData.forEach((value) => {
            result[value.department] = result[value.department] || 0;

            if (value.fired === true && value.post === post) {
                ++result[value.department];
            }
        });

        return result;
    }

    firedAmount(isFired) {
        let result = [];

        this.restorauntData.forEach((value) => {
            result[value.department] = result[value.department] || 0;

            if (value.fired === isFired) {
                ++result[value.department];
            }
        });

        return result;
    }

}

const restorauntResult = new RestorauntCalculation(employerArr);
