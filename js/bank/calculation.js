class BankCalculation {
    constructor(data) {
        this.bankData = data;
    }

    fetchData(data, callback) {
        return fetch('https://www.cbr-xml-daily.ru/daily_json.js').then(function (result) {
            return result.json();
        }).then(function (currencyArray) {
            let currency = {};

            currency.USD = currencyArray.Valute.USD.Previous;
            currency.EUR = currencyArray.Valute.EUR.Previous;
            currency.UAH = currencyArray.Valute.UAH.Previous;
            return callback(data, currency);
        });
    }

    countingMoney() {
        let allMoney = [];

        this.bankData.forEach((value) => {
            allMoney[value.credit.ownBalance.currency] = allMoney[value.credit.ownBalance.currency] || 0;
            allMoney[value.credit.ownBalance.currency] += value.credit.ownBalance.ownBalance;
            
            allMoney[value.credit.creditBalance.currency] = allMoney[value.credit.ownBalance.currency] || 0;
            allMoney[value.credit.creditBalance.currency] += value.credit.creditBalance.creditBalance;

            allMoney[value.debit.curentBalance] = allMoney[value.debit.curentBalance] || 0;
            allMoney[value.debit.curentBalance] += value.debit.curentBalance;
        });
        
        return this.fetchData(allMoney, (data, currency) => {
            let convertAllMoney = data.RUB;
            data.RUB = 0;

            for ( let index in currency ) {
                for ( let compareIndex in allMoney ) {
                    if ( index === compareIndex ) {
                        convertAllMoney += allMoney[compareIndex] * currency[index];
                    }
                }
            }
            
            return Math.round(convertAllMoney);
        });
    }

    countingBorowedMoney() {
        let borrowedMoney = [];

        this.bankData.forEach((value) => {
            borrowedMoney[value.credit.creditBalance.currency] = borrowedMoney[value.credit.creditBalance.currency] || 0;
            borrowedMoney[value.credit.creditBalance.currency] += value.credit.creditBalance.creditBalance;
        });

        return this.fetchData(borrowedMoney, (data, currency) => {
            let convertAllMoney = data.RUB;
            data.RUB = 0;

            for ( let index in currency ) {
                for ( let compareIndex in borrowedMoney ) {
                    if ( index === compareIndex ) {
                        convertAllMoney += borrowedMoney[compareIndex] * currency[index];
                    }
                }
            }

            return Math.round(convertAllMoney);
        });
    }


    countingBorowedMonyeInaciveUsers() {
        let inactive = [];

        this.bankData.forEach((value) => {
            inactive[value.credit.creditBalance.currency] = inactive[value.credit.creditBalance.currency] || 0;

            if ( value.activeUser === false ) {
                inactive[value.credit.creditBalance.currency] += value.credit.creditBalance.creditBalance;
            }
        });
        
        return this.fetchData(inactive, (data, currency) => {
            let convertAllMoney = data.RUB;
            data.RUB = 0;
            
            for ( let index in currency ) {
                for ( let compareIndex in inactive ) {
                    if ( index === compareIndex ) {
                        convertAllMoney += inactive[compareIndex] * currency[index];
                    }
                }
            }

            return Math.round(convertAllMoney);
        });
    } 
}

const bankResult = new BankCalculation(bankUsers);
